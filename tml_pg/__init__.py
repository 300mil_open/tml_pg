"""TML_PG
Python shell for Postgres manipulation

"""

import subprocess
import psycopg2
import psycopg2.extras as psy_extra
import json


__version__ = '2.0.7'
__author__ = 'Andre Resende'


def makeConf(user = 'postgres', password = 'postgres', host = 'localhost', port = 5432, database = 'postgres', file = None):
    r"""Creates a PostgreSQL configuration dict.

    Parameters
    ----------
    user : User name, str
        PostgreSQL user name.
    password : Password, str
        PostgreSQL user's password.
    host : Server host address, str
        PostgreSQL server host address.
    port : Server port, int
        PostgreSQL server host port.
    database : Database name, str
        PostgreSQL database name to connect to.
    File : File path, str
        JSON configuration file path to be used as configuration.

    Returns
    -------
    Configuration dict
        Dict keys: 'user': Postgres user as STRING, 'password': User password as STRING, 'host': Server host as STRING, 'port': Server port as INTEGER, 'database': Postgres database name as STRING.

    """

    if file:
        conf = json.loads(open(file).read())

    else:
        conf = {
            'user': user,
            'password': password,
            'host': host,
            'port': port,
            'database': database
        }


    return conf


def createDB(database_in, conf, schema = None, extension = None):
    r"""Create a PostgreSQL database.

    Parameters
    ----------
    database_in : Database name, str
        PostgreSQL database name which will be created.
    conf : PostgreSQL configuration, dict
        Dict keys: 'user': Postgres user as STRING, 'password': User password as STRING, 'host': Server host as STRING, 'port': Server port as INTEGER, 'database': Postgres database name as STRING.
    schema : Schema name, str or list
        List of names or single name to be created as PostgreSQL schema.
    extension : Extension name, str or list
        List of names or single name to be created as PostgreSQL extensions.

    Returns
    -------
    Configuration dict with the created database
        Dict keys: 'user': Postgres user as STRING, 'password': User password as STRING, 'host': Server host as STRING, 'port': Server port as INTEGER, 'database': Postgres database name as STRING.

    """
    conn = psycopg2.connect(database=conf["database"], user=conf["user"], password=conf["password"], host=conf["host"], port= conf["port"])
    conn.autocommit = True
    cursor = conn.cursor()
    cursor.execute(f"CREATE database {database_in};")
    print(f"Database {database_in} created successfully.")
    conn.close()

    confNewDB = makeConf(user = conf["user"], password = conf["password"], host = conf["host"], port = conf["port"], database = database_in, file = None)

    if schema:
        schema = [schema] if type(schema) is str(schema) else schema
        for sch in schema:
            runQuery(f"CREATE SCHEMA IF NOT EXISTS {sch}", confNewDB)
    if extension:
        extension = [extension] if type(extension) is str(extension) else extension
        for ext in extension:
            runQuery(f"CREATE EXTENSION {ext}", confNewDB)

    return confNewDB




def pg2spatialite(table_in, folder_out, conf, schema = 'public', query = None):
    r"""Export a PostgreSQL table to a Spatialite file.

    Parameters
    ----------
    table_in : Table name, str
        PostgreSQL table name which will be exported to Spatialite.
    folder_out : Database path, str
        Folder where the Spatialite file will be saved to.
    conf : PostgreSQL configuration, dict
        Dict keys: 'user': Postgres user as STRING, 'password': User password as STRING, 'host': Server host as STRING, 'port': Server port as INTEGER, 'database': Postgres database name as STRING.
    schema : Schema name, str
        PostgreSQL schema name where the table to be exported is stored.
    query : Query, str
        PostgreSQL query to generate a table to be exported. If None is passed the whole given table is exported as it is.

    """

    query = f'"SELECT * FROM {schema}.{table_in}"' if query is None else f'"{query}"'

    try:
        connect_command = f'ogr2ogr -f SQLite -dsco SPATIALITE=YES "{folder_out}{table_in}.sqlite" "PG:host={conf["host"]} user={conf["user"]} dbname={conf["database"]} active_schema={schema} schemas={schema} password={conf["password"]}" -sql {query} -nln {table_in}'

        print("Executing:", connect_command)
        process = subprocess.Popen(connect_command, shell=True)
        process.communicate()
        process.wait()
        print("Posgres table", table_in, "wrote to Spatialite db", table_in)

    except Exception as err:
        print("Failed to write from Spatialite to Postgres")
        raise


def pg2sqlite(table_in, folder_out, conf, schema = 'public', query = None):
    r"""Export a PostgreSQL table to a Spatialite file.

    Parameters
    ----------
    table_in : Table name, str
        PostgreSQL table name which will be exported to Spatialite.
    folder_out : Database path, str
        Folder where the Spatialite file will be saved to.
    conf : PostgreSQL configuration, dict
        Dict keys: 'user': Postgres user as STRING, 'password': User password as STRING, 'host': Server host as STRING, 'port': Server port as INTEGER, 'database': Postgres database name as STRING.
    schema : Schema name, str
        PostgreSQL schema name where the table to be exported is stored.
    query : Query, str
        PostgreSQL query to generate a table to be exported. If None is passed the whole given table is exported as it is.

    """

    query = f'"SELECT * FROM {schema}.{table_in}"' if query is None else f'"{query}"'

    try:
        connect_command = f'ogr2ogr -f SQLite "{folder_out}{table_in}.sqlite" "PG:host={conf["host"]} user={conf["user"]} dbname={conf["database"]} active_schema={schema} schemas={schema} password={conf["password"]}" -sql {query} -nln {table_in}'

        print("Executing:", connect_command)
        process = subprocess.Popen(connect_command, shell=True)
        process.communicate()
        process.wait()
        print("Posgres table", table_in, "wrote to Spatialite db", table_in)

    except Exception as err:
        print("Failed to write from Spatialite to Postgres")
        raise


def spatialite2pg(db, table_out, table_in, conf, schema = 'public', query = None):
    r"""Import a Spatialite file to a PostgreSQL table.

    Parameters
    ----------
    db : Database path, str
        Spatialite database path to be imported to Postgres.
    table_out : Table name, str
        Table name as where the database table will be imported to.
    table_in : Table name, str
        Table name from the Spatialite database that will be imported.
    conf : PostgreSQL configuration, dict
        Dict keys: 'user': Postgres user as STRING, 'password': User password as STRING, 'host': Server host as STRING, 'port': Server port as INTEGER, 'database': Postgres database name as STRING.
    schema : Schema name, str
        PostgreSQL schema name where the table will be exported in.
    query : Query, str
        PostgreSQL query to generate a table to be imported. If None is passed the whole given table is exported as it is.

    """

    query = f'"SELECT * FROM {table_in}"' if query is None else f'"{query}"'

    try:
        connect_command = f'ogr2ogr -f "PostgreSQL" "PG:host={conf["host"]} user={conf["user"]} dbname={conf["database"]} password={conf["password"]}" "{db}" -sql {query} -lco GEOMETRY_NAME=geom -lco SCHEMA={schema} -lco PRECISION=no -lco OVERWRITE=yes -nlt PROMOTE_TO_MULTI -nln {table_out} -overwrite'

        print("Executing:", connect_command)
        process = subprocess.Popen(connect_command, shell=True)
        process.communicate()
        process.wait()
        print("Spatialite file", db, "wrote to Postgres table", table_out)

    except Exception as err:
        print("Failed to write from Spatialite to Postgres")
        raise


def shp2pg(file, table_out, conf, schema = 'public'):
    r"""Import a Shapefile file to a PostgreSQL table.

    Parameters
    ----------
    file : File path, str
        Shapefile file path to be imported to Postgres.
    table_out : Table name, str
        Table name as where the database table will be imported to.
    conf : PostgreSQL configuration, dict
        Dict keys: 'user': Postgres user as STRING, 'password': User password as STRING, 'host': Server host as STRING, 'port': Server port as INTEGER, 'database': Postgres database name as STRING.
    schema : Schema name, str
        PostgreSQL schema name where the table will be exported in.

    """

    try:
        connect_command = f'ogr2ogr -f "PostgreSQL" "PG:host={conf["host"]} user={conf["user"]} dbname={conf["database"]} password={conf["password"]}" "{file}" -lco GEOMETRY_NAME=geom -lco SCHEMA={schema} -lco PRECISION=no -lco OVERWRITE=yes -nlt PROMOTE_TO_MULTI -nln {table_out} -overwrite'

        print("Executing:", connect_command)
        process = subprocess.Popen(connect_command, shell=True)
        process.communicate()
        process.wait()
        print("Shapefile", file, "wrote to Postgres table", table_out)

    except Exception as err:
        print("Failed to write from Shapefile to Postgres")
        raise


def shp2spatialite(file, file_out, table_out):
    r"""Converts a Shapefile file to a Spatialite file.

    Parameters
    ----------
    file : File path, str
        Shapefile file path to be imported to Postgres.
    file_out : File path, str
        Spatialite file path where the Shapefile will be exported to.

    """

    try:
        connect_command = f'ogr2ogr -f SQLite -dsco SPATIALITE=YES "{file_out}" "{file}" -nlt PROMOTE_TO_MULTI -nln {table_out}'

        print("Executing:", connect_command)
        process = subprocess.Popen(connect_command, shell=True)
        process.communicate()
        process.wait()
        print("Shapefile", file, "wrote to Spatialite file", table_out, "in the table", table_out)

    except Exception as err:
        print("Failed to write from Shapefile to Spatialite")
        raise


def runQuery(query, conf):
    r"""Run a PostgreSQL query.

    Parameters
    ----------
    query : Query, str
        PostgreSQL query to generate a table to be imported. If None is passed the whole given table is exported as it is.
    conf : PostgreSQL configuration, dict
        Dict keys: 'user': Postgres user as STRING, 'password': User password as STRING, 'host': Server host as STRING, 'port': Server port as INTEGER, 'database': Postgres database name as STRING.

    Returns
    -------
    Query result
        List of dicts where each list item is a returned row and each key from the list a column with their respective values.

    """

    rec = None

    try:
        conn = psycopg2.connect(f"dbname = '{conf['database']}' user = '{conf['user']}' host = '{conf['host']}' password = '{conf['password']}'")
        cur = conn.cursor(cursor_factory = psy_extra.DictCursor)

    except Exception as e:
        print ("I am unable to connect to the database", str(e))

    try:
        cur.execute(query)
        record = cur.fetchall()

        rec = []

        for i in record:
            rec.append(dict(i))

        cur.close()

    except Exception as e:
        print (str(e))

    if conn is not None:
        conn.commit()
        conn.close()

    return rec


def createTable(table_in, col_dict, conf, schema = 'public', primary_key='pk', drop = False):
    r"""Run a PostgreSQL query.

    Parameters
    ----------
    table_in : Table name, str
        PostgreSQL table name that will be created.
    col_dict : Table's column, dict
        Dict where keys are columns names and values are column types.
    conf : PostgreSQL configuration, dict
        Dict keys: 'user': Postgres user as STRING, 'password': User password as STRING, 'host': Server host as STRING, 'port': Server port as INTEGER, 'database': Postgres database name as STRING.
    schema : Schema name, str
        PostgreSQL schema name where the table will be created in.
    primary_key : Column name, str
        Column name to be used as the table's PRIMARY KEY, must be unique if a table's column is passed else a PK column is created.
    drop : Create statement option, boolean
        Drops an existing table with the same given name.

    """

    if primary_key=='pk':
        pk = 'pk BIGSERIAL PRIMARY KEY'
    else:
        pk = f'PRIMARY KEY ({primary_key})'

    if drop:
        q1 = f"DROP TABLE IF EXISTS {schema}.{table_in};"

    cols = [f'{x} {col_dict[x]}' for x in col_dict]
    cols = ', '.join(cols)

    query = f'''
        CREATE TABLE IF NOT EXISTS {schema}.{table_in} (
            {pk}, {cols}
        );
    '''
    query = q1+query if drop else query

    runQuery(query, conf)

    print(f'Table {table_in} created.')


def insertData(row_list, table_in, conf, schema = 'public', on_conflict = False, conlict_col = None):
    r"""Insert data to a PostgreSQL table.

    Parameters
    ----------
    row_list : Data, list
        List of dicts where each dict is a row and each key is a column with value as its value.
    table_in : Table's name, str
        PostgreSQL table's name where the data will be inserted into.
    conf : PostgreSQL configuration, dict
        Dict keys: 'user': Postgres user as STRING, 'password': User password as STRING, 'host': Server host as STRING, 'port': Server port as INTEGER, 'database': Postgres database name as STRING.
    schema : Schema name, str
        PostgreSQL schema name where the table is stored.
    on_conflict : Insert options, boolean
        Updates existing rows when a given CONFLICT_ROW value is already in the table.
    conlict_col : Column name, str
        Unique value column name to be used when ON_CONFLICT is True.

    """

    cols = ', '.join([str(d) for d in row_list[0].keys()])
    values = [list(d.values()) for d in row_list]
    values = [f"({', '.join([str(x) for x in v])})" for v in values]
    values = ', '.join(values)

    query = f'''
    INSERT INTO {schema}.{table_in} ({cols})
    VALUES {values}
    '''

    if on_conflict:
        updt = ', '.join([f"{col} = excluded.{col}" for col in [str(d) for d in row_list[0].keys()] if col != conlict_col])

        conflict = f'''
            ON CONFLICT ({conlict_col}) 
            DO UPDATE SET 
            {updt};
        '''

        query = f"{query} {conflict}"

    runQuery(query, conf)

    print(f'{len(row_list)} entries inserted into {table_in}.')


####################################################################################################


####################################################################################################

if __name__ == '__main__':
    print()
