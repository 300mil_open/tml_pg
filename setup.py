from setuptools import setup

setup(
    name='tml_pg',
    version='2.0.7',
    description='Python shell for Postgres manipulation',
    url='https://gitlab.com/300mil/tml_pg.git',
    author='Andre Resende',
    author_email='dedsresende@gmail.com',
    license='unlicense',
    packages=['tml_pg'],
    zip_safe=False
)
